#!/usr/bin/env node
import * as cdk from 'aws-cdk-lib';
import { TestCircleCiStack } from '../lib/test_circle_ci-stack';

const app = new cdk.App();
new TestCircleCiStack(app, 'TestCircleCiStack');
